package INF102.lab1.triplicate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MyTriplicate<T extends Comparable<T>> implements ITriplicate<T>  {

    
    
    @Override
    public T findTriplicate(List<T> list) {
        Collections.sort(list);
        Integer n = list.size();
        List<T> triples = new ArrayList<T>();
        for (int i = 0; i < n-2; i++) {
			if (list.get(i).equals(list.get(i+1)) && list.get(i+1).equals(list.get(i+2))){
                return list.get(i);
            }

        }
        return null;
    }
    
}

